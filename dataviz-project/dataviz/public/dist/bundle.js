(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Backbone.Collection.extend({
    url: 'http://172.18.33.116:3000/pays',

    useDiplomaUrl: function useDiplomaUrl(name) {
        this.url = 'http://172.18.33.116:3000/pays/' + name;
    }
});

},{}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _personneCollection = require('./personneCollection');

var _personneCollection2 = _interopRequireDefault(_personneCollection);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var personnView = Backbone.View.extend({

    //tagName: 'main',
    el: $('main'),

    initialize: function initialize() {
        this.collection = new _personneCollection2.default();
        this.collection.on('reset', this.render.bind(this));
        this.collection.fetch({ reset: true });
        //this.listenTo(this.collection, 'sync', this.render);
    },

    render: function render() {
        var that = this;
        //console.debug(this.collection.toJSON());
        $.get("./src/personnes/templates/personneTemplate.html", function (res) {
            //var html = $(res);

            var template = Handlebars.compile(res);

            var html = template({ personnes: that.collection.toJSON() }); // +/- donne un nom a mon obj

            that.$el.html(html);
            //console.log(html);
            //console.log(that.collection.toJSON());

            $.get("./src/vendor/assets/couronne-ext.svg", function (svg) {

                var svgElement = $(svg).find('svg');
                $('#nav-center').append(svgElement);

                d3.selectAll('.arc').on('click', function (e) {
                    //console.log(this.id);

                    this.classList.toggle('selected');
                    that.renderDiploma();
                });
                var svg = d3.select("#circles").append("svg");
            });

            $.get("./src/vendor/assets/logo-gob-white.svg", function (svg) {
                var svgElement = $(svg).find('svg');
                $('#logo').append(svgElement);
            });

            /*ADD*/
            $.get("./src/vendor/assets/couronne-date.svg", function (svg) {
                var svgElement = $(svg).find('svg');
                $('#date').append(svgElement);
            });

            that.renderSVG();
            that.renderAnimation();

            setTimeout(that.afterRender.bind(that), 1);
        });

        return this;
    },

    renderDiploma: function renderDiploma() {

        var diplomas = [];
        d3.selectAll('.selected').each(function () {
            diplomas.push($(this).attr('data-diploma'));
        });
        this.collection.useDiplomaUrl(diplomas.join('-'));
        this.collection.fetch({ reset: false }).done(this.renderSVG.bind(this));
    },
    afterRender: function afterRender() {
        this.renderSVG();
    },
    renderSVG: function renderSVG() {
        var svg = d3.select("#circles").select("svg");
        var shapes = svg.selectAll(".shapes").data(this.collection.toJSON(), function (d) {
            return d.pays;
        });
        //console.table(this.collection.toJSON());

        var angle = 0;
        var radius = 275;
        var nbr = this.collection.toJSON().length;
        var width = window.innerWidth;
        var height = window.innerHeight;
        var arc = $(".selected").attr('id');

        var group = shapes.enter().append('g').classed('shapes', true);
        shapes.exit().remove();
        //console.log(group);

        var color = '#1B6DAD';
        var stroke;
        group.append("circle").attr("cx", function (d, i) {
            return Math.sin(i + 1 * 480 * Math.PI / 180) * radius + width / 2.05;
        }) //return (i+1) * 55;
        .attr("cy", function (d, i) {
            return Math.cos(i + 1 * 480 * Math.PI / 180) * radius + height / 2;
        }).attr("r", function (d, i) {
            return Math.log(d.total) * 5 + 3;
        }).style("fill", function (d) {
            if (Math.log(d.total) * 5 + 3 < 4) {
                // 3 = valeur minimum = 1 sur l'echelle de log
                stroke = "#FFF";
            } else {
                stroke = '#FFF';
            }
            return stroke;
        }).style("stroke", '#000').on("click", function (d) {

            //alert("view linkedin profile")
            $("#bulles").fadeIn();
            setTimeout(function () {
                $("#bulles").fadeOut();
            }, 5000);
        });

        group.append("text").attr("dx", function (d, i) {
            return Math.sin(i + 1 * 480 * Math.PI / 180) * radius * 1.3 + width / 2.15;
        }).attr("dy", function (d, i) {
            return Math.cos(i + 1 * 480 * Math.PI / 180) * radius * 1.25 + height / 2;
        }).text(function (d) {
            return d.pays;
        }).style("fill", '#FFF');

        shapes.select('circle').transition().duration(800).ease('bounce').attr("r", function (d, i) {
            return Math.log(d.total) * 6 + 3;
        }).style("fill", function (d) {
            if (Math.log(d.total) * 5 + 3 < 1) {
                // 3 = valeur minimum = 1 sur l'echelle de log
                console.log(Math.log(d.total) * 5 + 3);
                stroke = "#00";
            } else {
                if (arc == 1) {
                    stroke = "#00FFC1";
                }
                if (arc == 2) {
                    stroke = "#00C4BB";
                }
                if (arc == 3) {
                    stroke = "#00A2B8";
                }
                if (arc == 4) {
                    stroke = "#0082B5";
                }
                if (arc == 5) {
                    stroke = "#0063B2";
                }
                if (arc == 6) {
                    stroke = "#0040c1";
                }
            }
            return stroke;
        });

        shapes.selectAll('circle, text').style("fill", function (d) {

            if (arc === undefined) {
                color = "#FFF";
            }
            if (arc == 1) {
                color = "#00FFC1";
            }
            if (arc == 2) {
                color = "#00C4BB";
            }
            if (arc == 3) {
                color = "#00A2B8";
            }
            if (arc == 4) {
                color = "#0082B5";
            }
            if (arc == 5) {
                color = "#0063B2";
            }
            if (arc == 6) {
                color = "#0040c1";
            }

            return color;
        });

        var element = $(".selected");
        //console.log($(".selected").attr('id'))
        if (element.length) {
            $("#circles").css("opacity", "1");
        }
        if (!element.length) {
            $("#circles").animate({ "opacity": "0.6" }, 200);
        }

        if (element[0] === undefined) {
            $('#info').text("La repartition international de l'ensemble des Gobelins").css("color", "#104168");
        }

        var diplomas = [];
        d3.selectAll('.selected').each(function () {
            diplomas.push($(this).attr('data-diploma'));
        });
        //console.log(diplomas)

        //$('#info').text('La répartition internationalle des ');
        //$('#info').append( diplomas +" en 2015");

        if (element[0] !== undefined) {
            $('#info').text('La répartition international des ');
            $('#info').append(diplomas + " en 2015");
        }
    },
    renderAnimation: function renderAnimation() {

        var flag = 0;
        var flag2 = 0;

        $('#pictoadd').click(function () {
            flag = 1;

            $('body').css("background-image", "none");

            if (flag2 == 0) {
                $('#login').fadeIn();
                flag2 = 1;
            } else {
                $('#login').fadeOut();
                $('body').css("background-image", "url('../src/vendor/assets/background.png')");
                $('#pictoadd').fadeOut();
                $('main').fadeIn(2000);
            }

            console.log("picto");
        });

        if (flag == 0) {
            $(document).click(function () {

                if (!event.preventDefault() && flag == 1) return;

                flag = 1;
                $('body').css("background-image", "url('../src/vendor/assets/background.png')");
                $('#pictoadd').fadeOut();
                $('main').fadeIn(2000);
                //console.log(flag);
            });
        }
    }
});

exports.default = personnView;

},{"./personneCollection":1}],3:[function(require,module,exports){
'use strict';

var _personneView = require('./personnes/personneView');

var _personneView2 = _interopRequireDefault(_personneView);

function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : { default: obj };
}

$(function () {
        var view = new _personneView2.default();
});

},{"./personnes/personneView":2}]},{},[3])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Vzci9sb2NhbC9saWIvbm9kZV9tb2R1bGVzL3dhdGNoaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJwdWJsaWMvc3JjL3BlcnNvbm5lcy9wZXJzb25uZUNvbGxlY3Rpb24uanMiLCJwdWJsaWMvc3JjL3BlcnNvbm5lcy9wZXJzb25uZVZpZXcuanMiLCJwdWJsaWMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7OztrQkNBZSxRQUFRLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxBQUN0QztPQUFHLEVBQUUsZ0NBQWdDLEFBR3JDOztpQkFBYSx5QkFBQyxJQUFJLEVBQUUsQUFDbkI7WUFBSSxDQUFDLEdBQUcsR0FBRyxpQ0FBaUMsR0FBRyxJQUFJLENBQUM7S0FDcEQ7Q0FFSixDQUFDOzs7Ozs7Ozs7Ozs7Ozs7OztBQ0xGLElBQUksV0FBVyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEFBR25DOzs7TUFBRSxFQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQUFFWjs7Y0FBVSxFQUFFLHNCQUFVLEFBQ2xCO1lBQUksQ0FBQyxVQUFVLEdBQUcsa0NBQXdCLENBQUMsQUFDM0M7WUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQUFDcEQ7WUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFDLENBQUM7O0FBQUMsS0FFeEMsQUFHRDs7VUFBTSxFQUFFLGtCQUFVLEFBQ2Q7WUFBSSxJQUFJLEdBQUcsSUFBSTs7QUFBQyxBQUVoQixTQUFDLENBQUMsR0FBRyxDQUFDLGlEQUFpRCxFQUFFLFVBQVMsR0FBRyxFQUFDLEFBR2xFOzs7Z0JBQUksUUFBUSxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQUFFdkM7O2dCQUFJLElBQUksR0FBRyxRQUFRLENBQUUsRUFBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsRUFBQyxDQUFFOztBQUFDLEFBRTdELGdCQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Ozs7QUFBQyxBQUlwQixhQUFDLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxFQUFFLFVBQVMsR0FBRyxFQUFDLEFBRXZEOztvQkFBSSxVQUFVLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxBQUNwQztpQkFBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE1BQU0sQ0FBRSxVQUFVLENBQUUsQ0FBQyxBQUV0Qzs7a0JBQUUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFTLENBQUMsRUFBQyxBQUd4Qzs7O3dCQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxBQUNsQzt3QkFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO2lCQUV4QixDQUFDLENBQUMsQUFDSDtvQkFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7YUFFakQsQ0FBQyxDQUFDLEFBRUY7O2FBQUMsQ0FBQyxHQUFHLENBQUMsd0NBQXdDLEVBQUUsVUFBUyxHQUFHLEVBQUMsQUFDMUQ7b0JBQUksVUFBVSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQUFDcEM7aUJBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLENBQUUsVUFBVSxDQUFFLENBQUM7YUFDbkMsQ0FBQzs7O0FBQUMsQUFHQyxhQUFDLENBQUMsR0FBRyxDQUFDLHVDQUF1QyxFQUFFLFVBQVMsR0FBRyxFQUFDLEFBQ3hEO29CQUFJLFVBQVUsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEFBQ3BDO2lCQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxDQUFFLFVBQVUsQ0FBRSxDQUFDO2FBQ25DLENBQUMsQ0FBQyxBQUVQOztnQkFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLEFBQ2pCO2dCQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsQUFFdkI7O3NCQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDOUMsQ0FBQyxDQUFDLEFBRUg7O2VBQU8sSUFBSSxDQUFDO0tBQ2YsQUFFRDs7aUJBQWEsMkJBQUcsQUFFWjs7WUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEFBQ2xCO1VBQUUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVUsQUFDckM7b0JBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1NBQy9DLENBQUMsQ0FBQyxBQUNIO1lBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxBQUNsRDtZQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFDLEtBQUssRUFBRSxLQUFLLEVBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0tBRXpFLEFBRUE7ZUFBVyx5QkFBRSxBQUNWO1lBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztLQUVwQixBQUVEO2FBQVMsdUJBQUcsQUFDUjtZQUFJLEdBQUcsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxBQUM5QztZQUFJLE1BQU0sR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsRUFBRSxVQUFTLENBQUMsRUFBQyxBQUN2QzttQkFBTyxDQUFDLENBQUMsSUFBSSxDQUFDO1NBQ2pCLENBQUM7OztBQUFDLEFBR1AsWUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEFBQ2Q7WUFBTSxNQUFNLEdBQUcsR0FBRyxDQUFDLEFBQ25CO1lBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsTUFBTSxDQUFDLEFBQzFDO1lBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsQUFDOUI7WUFBSSxNQUFNLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxBQUNoQztZQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEFBRXBDOztZQUFJLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUMsQUFDL0Q7Y0FBTSxDQUFDLElBQUksRUFBRSxDQUFDLE1BQU0sRUFBRTs7O0FBQUMsQUFHdkIsWUFBSSxLQUFLLEdBQUcsU0FBUyxDQUFDLEFBQ3RCO1lBQUksTUFBTSxDQUFDLEFBQ1g7YUFBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FDakIsSUFBSSxDQUFDLElBQUksRUFBRSxVQUFTLENBQUMsRUFBRSxDQUFDLEVBQUMsQUFBRTttQkFBTyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBQyxDQUFDLEdBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxFQUFFLEdBQUUsR0FBRyxDQUFFLEdBQUcsTUFBTSxHQUFHLEtBQUssR0FBQyxJQUFJLENBQUM7U0FBRTtBQUFDLFNBQzdGLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBUyxDQUFDLEVBQUUsQ0FBQyxFQUFDLEFBQUU7bUJBQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUMsQ0FBQyxHQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsRUFBRSxHQUFFLEdBQUcsQ0FBQyxHQUFHLE1BQU0sR0FBRyxNQUFNLEdBQUMsQ0FBQyxDQUFDO1NBQUUsQ0FBQyxDQUMxRixJQUFJLENBQUMsR0FBRyxFQUFFLFVBQVMsQ0FBQyxFQUFFLENBQUMsRUFBQyxBQUFFO21CQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFDLENBQUMsR0FBQyxDQUFDLENBQUE7U0FBRSxDQUFDLENBQ3pELEtBQUssQ0FBQyxNQUFNLEVBQUUsVUFBUyxDQUFDLEVBQUMsQUFDdEI7Z0JBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUMsQ0FBQyxHQUFDLENBQUMsR0FBRyxDQUFDLEVBQUMsQUFDMUI7O3NCQUFNLEdBQUcsTUFBTSxDQUFBO2FBQ2xCLE1BQUksQUFDRDtzQkFBTSxHQUFHLE1BQU0sQ0FBQTthQUNsQixBQUNEO21CQUFPLE1BQU0sQ0FBQTtTQUNoQixDQUFDLENBQ0QsS0FBSyxDQUFDLFFBQVEsRUFBQyxNQUFNLENBQUMsQ0FDdEIsRUFBRSxDQUFDLE9BQU8sRUFBQyxVQUFTLENBQUMsRUFBQyxBQUdmOzs7YUFBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEFBQzFCO3NCQUFVLENBQUMsWUFBVSxBQUNqQjtpQkFBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQzFCLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FFWixDQUFDLENBQUMsQUFFUDs7YUFBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FDZixJQUFJLENBQUMsSUFBSSxFQUFFLFVBQVMsQ0FBQyxFQUFFLENBQUMsRUFBQyxBQUFFO21CQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFDLENBQUMsR0FBQyxHQUFHLEdBQUksSUFBSSxDQUFDLEVBQUUsR0FBRSxHQUFHLENBQUUsR0FBRyxNQUFNLEdBQUMsR0FBRyxHQUFHLEtBQUssR0FBQyxJQUFJLENBQUU7U0FBRSxDQUFDLENBQ25HLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBUyxDQUFDLEVBQUUsQ0FBQyxFQUFDLEFBQUU7bUJBQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUMsQ0FBQyxHQUFDLEdBQUcsR0FBSSxJQUFJLENBQUMsRUFBRSxHQUFFLEdBQUcsQ0FBQyxHQUFHLE1BQU0sR0FBQyxJQUFJLEdBQUcsTUFBTSxHQUFDLENBQUMsQ0FBRTtTQUFFLENBQUMsQ0FDakcsSUFBSSxDQUFDLFVBQVMsQ0FBQyxFQUFDLEFBQ2I7bUJBQU8sQ0FBQyxDQUFDLElBQUksQ0FBQTtTQUNoQixDQUFDLENBQ0QsS0FBSyxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQyxBQUczQjs7Y0FBTSxDQUNELE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FDaEIsVUFBVSxFQUFFLENBQ1osUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUNiLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FDZCxJQUFJLENBQUMsR0FBRyxFQUFFLFVBQVMsQ0FBQyxFQUFFLENBQUMsRUFBQyxBQUFFO21CQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFDLENBQUMsR0FBQyxDQUFDLENBQUE7U0FBRSxDQUFDLENBQ3pELEtBQUssQ0FBQyxNQUFNLEVBQUUsVUFBUyxDQUFDLEVBQUMsQUFDdEI7Z0JBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUMsQ0FBQyxHQUFDLENBQUMsR0FBRyxDQUFDLEVBQUMsQUFDMUI7O3VCQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFDLENBQUMsR0FBQyxDQUFDLENBQUMsQ0FBQSxBQUNsQztzQkFBTSxHQUFFLEtBQUssQ0FBQTthQUNoQixNQUFJLEFBQ0w7b0JBQUksR0FBRyxJQUFJLENBQUMsRUFBQyxBQUFFOzBCQUFNLEdBQUcsU0FBUyxDQUFBO2lCQUFFLEFBQ25DO29CQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUMsQUFBRTswQkFBTSxHQUFHLFNBQVMsQ0FBQTtpQkFBRSxBQUNuQztvQkFBSSxHQUFHLElBQUksQ0FBQyxFQUFDLEFBQUU7MEJBQU0sR0FBRyxTQUFTLENBQUE7aUJBQUUsQUFDbkM7b0JBQUksR0FBRyxJQUFJLENBQUMsRUFBQyxBQUFFOzBCQUFNLEdBQUcsU0FBUyxDQUFBO2lCQUFFLEFBQ25DO29CQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUMsQUFBRTswQkFBTSxHQUFHLFNBQVMsQ0FBQTtpQkFBRSxBQUNuQztvQkFBSSxHQUFHLElBQUksQ0FBQyxFQUFDLEFBQUU7MEJBQU0sR0FBRyxTQUFTLENBQUE7aUJBQUU7YUFDbEMsQUFDRDttQkFBTyxNQUFNLENBQUE7U0FDaEIsQ0FBQyxDQUFDLEFBR1A7O2NBQU0sQ0FDRCxTQUFTLENBQUMsY0FBYyxDQUFDLENBQ3pCLEtBQUssQ0FBQyxNQUFNLEVBQUUsVUFBUyxDQUFDLEVBQUMsQUFFdEI7O2dCQUFJLEdBQUcsS0FBSyxTQUFTLEVBQUMsQUFBRTtxQkFBSyxHQUFHLE1BQU0sQ0FBQTthQUFFLEFBQ3hDO2dCQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUMsQUFBRTtxQkFBSyxHQUFHLFNBQVMsQ0FBQTthQUFFLEFBQ2xDO2dCQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUMsQUFBRTtxQkFBSyxHQUFHLFNBQVMsQ0FBQTthQUFFLEFBQ2xDO2dCQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUMsQUFBRTtxQkFBSyxHQUFHLFNBQVMsQ0FBQTthQUFFLEFBQ2xDO2dCQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUMsQUFBRTtxQkFBSyxHQUFHLFNBQVMsQ0FBQTthQUFFLEFBQ2xDO2dCQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUMsQUFBRTtxQkFBSyxHQUFHLFNBQVMsQ0FBQTthQUFFLEFBQ2xDO2dCQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUMsQUFBRTtxQkFBSyxHQUFHLFNBQVMsQ0FBQTthQUFFLEFBRWxDOzttQkFBTyxLQUFLLENBQUE7U0FDZixDQUFDLENBQUMsQUFJSDs7WUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLFdBQVcsQ0FBQzs7QUFBQyxBQUU3QixZQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUMsQUFDZDthQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsR0FBRyxDQUFFLFNBQVMsRUFBRSxHQUFHLENBQUUsQ0FBQztTQUN2QyxBQUNEO1lBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFDLEFBQ2Y7YUFBQyxDQUFFLFVBQVUsQ0FBRSxDQUFDLE9BQU8sQ0FBQyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN0RCxBQUdEOztZQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxTQUFTLEVBQUUsQUFDeEI7YUFBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyx5REFBeUQsQ0FBQyxDQUFDLEdBQUcsQ0FBRSxPQUFPLEVBQUUsU0FBUyxDQUFFLENBQUM7U0FDekcsQUFFRDs7WUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEFBQ2Q7VUFBRSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBVSxBQUN4QztvQkFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7U0FDL0MsQ0FBQzs7Ozs7O0FBQUMsQUFNSixZQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxTQUFTLEVBQUUsQUFDMUI7YUFBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLEFBQ3JEO2FBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLENBQUUsUUFBUSxHQUFFLFVBQVUsQ0FBQyxDQUFDO1NBQzVDO0tBTVIsQUFBRzttQkFBZSw2QkFBRyxBQUVkOztZQUFJLElBQUksR0FBRyxDQUFDLENBQUMsQUFDYjtZQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsQUFFZDs7U0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxZQUFVLEFBQzNCO2dCQUFJLEdBQUcsQ0FBQyxDQUFDLEFBRVQ7O2FBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUUsa0JBQWtCLEVBQUUsTUFBTSxDQUFFLENBQUMsQUFFNUM7O2dCQUFHLEtBQUssSUFBRyxDQUFDLEVBQUMsQUFDVDtpQkFBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEFBQ3JCO3FCQUFLLEdBQUcsQ0FBQyxDQUFDO2FBQ2IsTUFBSSxBQUNEO2lCQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsQUFDdEI7aUJBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUUsa0JBQWtCLEVBQUUsNENBQTRDLENBQUUsQ0FBQyxBQUNsRjtpQkFBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDLEFBQ3pCO2lCQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzFCLEFBRUQ7O21CQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBRXhCLENBQUMsQ0FBQyxBQUlIOztZQUFHLElBQUksSUFBSSxDQUFDLEVBQUMsQUFDVDthQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLFlBQVUsQUFFeEI7O29CQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxJQUFJLElBQUksSUFBSSxDQUFDLEVBQUUsT0FBTyxBQUVqRDs7b0JBQUksR0FBRyxDQUFDLENBQUMsQUFDVDtpQkFBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBRSxrQkFBa0IsRUFBRSw0Q0FBNEMsQ0FBRSxDQUFDLEFBQ2xGO2lCQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsQUFDekI7aUJBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDOztBQUFDLGFBRTFCLENBQUMsQ0FBQTtTQUNMO0tBUVI7Q0FLSixDQUFDLENBQUM7O2tCQUVZLFdBQVc7Ozs7Ozs7Ozs7Ozs7QUM5UDFCLENBQUMsQ0FBQyxZQUFXLEFBQ0w7WUFBSSxJQUFJLEdBQUcsNEJBQWtCLENBQUM7Q0FDcEMsQ0FBQyxDQUFDIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsImV4cG9ydCBkZWZhdWx0IEJhY2tib25lLkNvbGxlY3Rpb24uZXh0ZW5kKHtcbiAgICB1cmw6ICdodHRwOi8vMTcyLjE4LjMzLjExNjozMDAwL3BheXMnLFxuXG4gICAgXG4gICAgdXNlRGlwbG9tYVVybChuYW1lKSB7XG4gICAgXHR0aGlzLnVybCA9ICdodHRwOi8vMTcyLjE4LjMzLjExNjozMDAwL3BheXMvJyArIG5hbWU7XG4gICAgfVxuXG59KSIsImltcG9ydCBwZXJzb25uZUNvbGxlY3Rpb24gZnJvbSAnLi9wZXJzb25uZUNvbGxlY3Rpb24nO1xuXG5cbnZhciBwZXJzb25uVmlldyA9IEJhY2tib25lLlZpZXcuZXh0ZW5kKHtcblxuICAgIC8vdGFnTmFtZTogJ21haW4nLFxuICAgIGVsOiQoJ21haW4nKSxcblxuICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uKCl7XG4gICAgICAgIHRoaXMuY29sbGVjdGlvbiA9IG5ldyBwZXJzb25uZUNvbGxlY3Rpb24oKTtcbiAgICAgICAgdGhpcy5jb2xsZWN0aW9uLm9uKCdyZXNldCcsIHRoaXMucmVuZGVyLmJpbmQodGhpcykpOyAgICAgXG4gICAgICAgIHRoaXMuY29sbGVjdGlvbi5mZXRjaCh7cmVzZXQ6IHRydWV9KTsgIFxuICAgICAgICAvL3RoaXMubGlzdGVuVG8odGhpcy5jb2xsZWN0aW9uLCAnc3luYycsIHRoaXMucmVuZGVyKTtcbiAgICB9LFxuXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uKCl7XG4gICAgICAgIHZhciB0aGF0ID0gdGhpcztcbiAgICAgICAgLy9jb25zb2xlLmRlYnVnKHRoaXMuY29sbGVjdGlvbi50b0pTT04oKSk7XG4gICAgICAgICQuZ2V0KFwiLi9zcmMvcGVyc29ubmVzL3RlbXBsYXRlcy9wZXJzb25uZVRlbXBsYXRlLmh0bWxcIiwgZnVuY3Rpb24ocmVzKXtcbiAgICAgICAgICAgIC8vdmFyIGh0bWwgPSAkKHJlcyk7XG5cbiAgICAgICAgICAgIHZhciB0ZW1wbGF0ZSA9IEhhbmRsZWJhcnMuY29tcGlsZShyZXMpO1xuXG4gICAgICAgICAgICB2YXIgaHRtbCA9IHRlbXBsYXRlKCB7cGVyc29ubmVzOiB0aGF0LmNvbGxlY3Rpb24udG9KU09OKCl9ICk7ICAgLy8gKy8tIGRvbm5lIHVuIG5vbSBhIG1vbiBvYmpcblxuICAgICAgICAgICAgdGhhdC4kZWwuaHRtbChodG1sKTtcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coaHRtbCk7XG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKHRoYXQuY29sbGVjdGlvbi50b0pTT04oKSk7XG5cbiAgICAgICAgICAgICQuZ2V0KFwiLi9zcmMvdmVuZG9yL2Fzc2V0cy9jb3Vyb25uZS1leHQuc3ZnXCIsIGZ1bmN0aW9uKHN2Zyl7XG5cbiAgICAgICAgICAgICAgICB2YXIgc3ZnRWxlbWVudCA9ICQoc3ZnKS5maW5kKCdzdmcnKTtcbiAgICAgICAgICAgICAgICAkKCcjbmF2LWNlbnRlcicpLmFwcGVuZCggc3ZnRWxlbWVudCApO1xuXG4gICAgICAgICAgICAgICAgZDMuc2VsZWN0QWxsKCcuYXJjJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSl7XG4gICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2codGhpcy5pZCk7XG4gICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbGFzc0xpc3QudG9nZ2xlKCdzZWxlY3RlZCcpO1xuICAgICAgICAgICAgICAgICAgICB0aGF0LnJlbmRlckRpcGxvbWEoKTtcblxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHZhciBzdmcgPSBkMy5zZWxlY3QoXCIjY2lyY2xlc1wiKS5hcHBlbmQoXCJzdmdcIik7XG5cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgJC5nZXQoXCIuL3NyYy92ZW5kb3IvYXNzZXRzL2xvZ28tZ29iLXdoaXRlLnN2Z1wiLCBmdW5jdGlvbihzdmcpe1xuICAgICAgICAgICAgICAgIHZhciBzdmdFbGVtZW50ID0gJChzdmcpLmZpbmQoJ3N2ZycpO1xuICAgICAgICAgICAgICAgICQoJyNsb2dvJykuYXBwZW5kKCBzdmdFbGVtZW50ICk7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIC8qQUREKi9cbiAgICAgICAgICAgICAgICAkLmdldChcIi4vc3JjL3ZlbmRvci9hc3NldHMvY291cm9ubmUtZGF0ZS5zdmdcIiwgZnVuY3Rpb24oc3ZnKXtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHN2Z0VsZW1lbnQgPSAkKHN2ZykuZmluZCgnc3ZnJyk7XG4gICAgICAgICAgICAgICAgICAgICQoJyNkYXRlJykuYXBwZW5kKCBzdmdFbGVtZW50ICk7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHRoYXQucmVuZGVyU1ZHKCk7XG4gICAgICAgICAgICB0aGF0LnJlbmRlckFuaW1hdGlvbigpO1xuXG4gICAgICAgICAgICBzZXRUaW1lb3V0KHRoYXQuYWZ0ZXJSZW5kZXIuYmluZCh0aGF0KSwgMSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH0sXG5cbiAgICByZW5kZXJEaXBsb21hKCkge1xuXG4gICAgICAgIHZhciBkaXBsb21hcyA9IFtdO1xuICAgICAgICBkMy5zZWxlY3RBbGwoJy5zZWxlY3RlZCcpLmVhY2goZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIGRpcGxvbWFzLnB1c2goJCh0aGlzKS5hdHRyKCdkYXRhLWRpcGxvbWEnKSk7XG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLmNvbGxlY3Rpb24udXNlRGlwbG9tYVVybChkaXBsb21hcy5qb2luKCctJykpO1xuICAgICAgICB0aGlzLmNvbGxlY3Rpb24uZmV0Y2goe3Jlc2V0OiBmYWxzZX0pLmRvbmUodGhpcy5yZW5kZXJTVkcuYmluZCh0aGlzKSk7XG5cbiAgICB9LFxuICAgIFxuICAgICBhZnRlclJlbmRlcigpe1xuICAgICAgICB0aGlzLnJlbmRlclNWRygpO1xuXG4gICAgfSxcblxuICAgIHJlbmRlclNWRygpIHtcbiAgICAgICAgdmFyIHN2ZyA9IGQzLnNlbGVjdChcIiNjaXJjbGVzXCIpLnNlbGVjdChcInN2Z1wiKTtcbiAgICAgICAgdmFyIHNoYXBlcyA9IHN2Zy5zZWxlY3RBbGwoXCIuc2hhcGVzXCIpXG4gICAgICAgICAgICAuZGF0YSh0aGlzLmNvbGxlY3Rpb24udG9KU09OKCksIGZ1bmN0aW9uKGQpe1xuICAgICAgICAgICAgICAgIHJldHVybiBkLnBheXM7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgLy9jb25zb2xlLnRhYmxlKHRoaXMuY29sbGVjdGlvbi50b0pTT04oKSk7XG5cbiAgICAgICAgdmFyIGFuZ2xlID0gMDtcbiAgICAgICAgY29uc3QgcmFkaXVzID0gMjc1O1xuICAgICAgICB2YXIgbmJyID0gdGhpcy5jb2xsZWN0aW9uLnRvSlNPTigpLmxlbmd0aDtcbiAgICAgICAgdmFyIHdpZHRoID0gd2luZG93LmlubmVyV2lkdGg7XG4gICAgICAgIHZhciBoZWlnaHQgPSB3aW5kb3cuaW5uZXJIZWlnaHQ7XG4gICAgICAgIHZhciBhcmMgPSAkKFwiLnNlbGVjdGVkXCIpLmF0dHIoJ2lkJyk7XG5cbiAgICAgICAgdmFyIGdyb3VwID0gc2hhcGVzLmVudGVyKCkuYXBwZW5kKCdnJykuY2xhc3NlZCgnc2hhcGVzJywgdHJ1ZSk7XG4gICAgICAgIHNoYXBlcy5leGl0KCkucmVtb3ZlKCk7XG4gICAgICAgIC8vY29uc29sZS5sb2coZ3JvdXApO1xuXG4gICAgICAgIHZhciBjb2xvciA9ICcjMUI2REFEJztcbiAgICAgICAgdmFyIHN0cm9rZTtcbiAgICAgICAgZ3JvdXAuYXBwZW5kKFwiY2lyY2xlXCIpXG4gICAgICAgICAgICAuYXR0cihcImN4XCIsIGZ1bmN0aW9uKGQsIGkpeyByZXR1cm4gTWF0aC5zaW4oaSsxKjQ4MCAqIE1hdGguUEkgLzE4MCApICogcmFkaXVzICsgd2lkdGgvMi4wNTsgfSkgICAgICAgICAvL3JldHVybiAoaSsxKSAqIDU1O1xuICAgICAgICAgICAgLmF0dHIoXCJjeVwiLCBmdW5jdGlvbihkLCBpKXsgcmV0dXJuIE1hdGguY29zKGkrMSo0ODAgKiBNYXRoLlBJIC8xODApICogcmFkaXVzICsgaGVpZ2h0LzI7IH0pXG4gICAgICAgICAgICAuYXR0cihcInJcIiwgZnVuY3Rpb24oZCwgaSl7IHJldHVybiBNYXRoLmxvZyhkLnRvdGFsKSo1KzMgfSlcbiAgICAgICAgICAgIC5zdHlsZShcImZpbGxcIiwgZnVuY3Rpb24oZCl7XG4gICAgICAgICAgICAgICAgaWYoIE1hdGgubG9nKGQudG90YWwpKjUrMyA8IDQpeyAgICAgLy8gMyA9IHZhbGV1ciBtaW5pbXVtID0gMSBzdXIgbCdlY2hlbGxlIGRlIGxvZ1xuICAgICAgICAgICAgICAgICAgICBzdHJva2UgPSBcIiNGRkZcIlxuICAgICAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgICAgICBzdHJva2UgPSAnI0ZGRidcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIHN0cm9rZVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5zdHlsZShcInN0cm9rZVwiLCcjMDAwJylcbiAgICAgICAgICAgIC5vbihcImNsaWNrXCIsZnVuY3Rpb24oZCl7IFxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgLy9hbGVydChcInZpZXcgbGlua2VkaW4gcHJvZmlsZVwiKVxuICAgICAgICAgICAgICAgICAgICAkKFwiI2J1bGxlc1wiKS5mYWRlSW4oKTsgXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpeyBcbiAgICAgICAgICAgICAgICAgICAgJChcIiNidWxsZXNcIikuZmFkZU91dCgpOyBcbiAgICAgICAgICAgICAgICB9LCA1MDAwKTtcblxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgZ3JvdXAuYXBwZW5kKFwidGV4dFwiKVxuICAgICAgICAgICAgLmF0dHIoXCJkeFwiLCBmdW5jdGlvbihkLCBpKXsgcmV0dXJuIE1hdGguc2luKGkrMSo0ODAgICogTWF0aC5QSSAvMTgwICkgKiByYWRpdXMqMS4zICsgd2lkdGgvMi4xNSA7IH0pXG4gICAgICAgICAgICAuYXR0cihcImR5XCIsIGZ1bmN0aW9uKGQsIGkpeyByZXR1cm4gTWF0aC5jb3MoaSsxKjQ4MCAgKiBNYXRoLlBJIC8xODApICogcmFkaXVzKjEuMjUgKyBoZWlnaHQvMiA7IH0pXG4gICAgICAgICAgICAudGV4dChmdW5jdGlvbihkKXtcbiAgICAgICAgICAgICAgICByZXR1cm4gZC5wYXlzXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnN0eWxlKFwiZmlsbFwiLCAnI0ZGRicpO1xuXG5cbiAgICAgICAgc2hhcGVzXG4gICAgICAgICAgICAuc2VsZWN0KCdjaXJjbGUnKVxuICAgICAgICAgICAgLnRyYW5zaXRpb24oKVxuICAgICAgICAgICAgLmR1cmF0aW9uKDgwMClcbiAgICAgICAgICAgIC5lYXNlKCdib3VuY2UnKVxuICAgICAgICAgICAgLmF0dHIoXCJyXCIsIGZ1bmN0aW9uKGQsIGkpeyByZXR1cm4gTWF0aC5sb2coZC50b3RhbCkqNiszIH0pXG4gICAgICAgICAgICAuc3R5bGUoXCJmaWxsXCIsIGZ1bmN0aW9uKGQpe1xuICAgICAgICAgICAgICAgIGlmKCBNYXRoLmxvZyhkLnRvdGFsKSo1KzMgPCAxKXsgICAgIC8vIDMgPSB2YWxldXIgbWluaW11bSA9IDEgc3VyIGwnZWNoZWxsZSBkZSBsb2dcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coTWF0aC5sb2coZC50b3RhbCkqNSszKVxuICAgICAgICAgICAgICAgICAgICBzdHJva2U9IFwiIzAwXCJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICBpZiggYXJjID09IDEpeyBzdHJva2UgPSBcIiMwMEZGQzFcIiB9XG4gICAgICAgICAgICAgICAgaWYoIGFyYyA9PSAyKXsgc3Ryb2tlID0gXCIjMDBDNEJCXCIgfVxuICAgICAgICAgICAgICAgIGlmKCBhcmMgPT0gMyl7IHN0cm9rZSA9IFwiIzAwQTJCOFwiIH1cbiAgICAgICAgICAgICAgICBpZiggYXJjID09IDQpeyBzdHJva2UgPSBcIiMwMDgyQjVcIiB9XG4gICAgICAgICAgICAgICAgaWYoIGFyYyA9PSA1KXsgc3Ryb2tlID0gXCIjMDA2M0IyXCIgfVxuICAgICAgICAgICAgICAgIGlmKCBhcmMgPT0gNil7IHN0cm9rZSA9IFwiIzAwNDBjMVwiIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIHN0cm9rZVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgIFxuXG4gICAgICAgIHNoYXBlc1xuICAgICAgICAgICAgLnNlbGVjdEFsbCgnY2lyY2xlLCB0ZXh0JylcbiAgICAgICAgICAgIC5zdHlsZShcImZpbGxcIiwgZnVuY3Rpb24oZCl7IFxuXG4gICAgICAgICAgICAgICAgaWYoIGFyYyA9PT0gdW5kZWZpbmVkKXsgY29sb3IgPSBcIiNGRkZcIiB9XG4gICAgICAgICAgICAgICAgaWYoIGFyYyA9PSAxKXsgY29sb3IgPSBcIiMwMEZGQzFcIiB9XG4gICAgICAgICAgICAgICAgaWYoIGFyYyA9PSAyKXsgY29sb3IgPSBcIiMwMEM0QkJcIiB9XG4gICAgICAgICAgICAgICAgaWYoIGFyYyA9PSAzKXsgY29sb3IgPSBcIiMwMEEyQjhcIiB9XG4gICAgICAgICAgICAgICAgaWYoIGFyYyA9PSA0KXsgY29sb3IgPSBcIiMwMDgyQjVcIiB9XG4gICAgICAgICAgICAgICAgaWYoIGFyYyA9PSA1KXsgY29sb3IgPSBcIiMwMDYzQjJcIiB9XG4gICAgICAgICAgICAgICAgaWYoIGFyYyA9PSA2KXsgY29sb3IgPSBcIiMwMDQwYzFcIiB9XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gY29sb3JcbiAgICAgICAgICAgIH0pO1xuXG5cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdmFyIGVsZW1lbnQgPSAkKFwiLnNlbGVjdGVkXCIpO1xuICAgICAgICAgICAgLy9jb25zb2xlLmxvZygkKFwiLnNlbGVjdGVkXCIpLmF0dHIoJ2lkJykpIFxuICAgICAgICAgICAgaWYoZWxlbWVudC5sZW5ndGgpe1xuICAgICAgICAgICAgICAgICQoXCIjY2lyY2xlc1wiKS5jc3MoIFwib3BhY2l0eVwiLCBcIjFcIiApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoIWVsZW1lbnQubGVuZ3RoKXtcbiAgICAgICAgICAgICAgICAkKCBcIiNjaXJjbGVzXCIgKS5hbmltYXRlKHsgXCJvcGFjaXR5XCI6IFwiMC42XCIgfSwgMjAwKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICBcbiAgICAgICAgICAgIGlmKGVsZW1lbnRbMF0gPT09IHVuZGVmaW5lZCApe1xuICAgICAgICAgICAgICAgICAkKCcjaW5mbycpLnRleHQoXCJMYSByZXBhcnRpdGlvbiBpbnRlcm5hdGlvbmFsIGRlIGwnZW5zZW1ibGUgZGVzIEdvYmVsaW5zXCIpLmNzcyggXCJjb2xvclwiLCBcIiMxMDQxNjhcIiApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICB2YXIgZGlwbG9tYXMgPSBbXTtcbiAgICAgICAgICAgICAgICBkMy5zZWxlY3RBbGwoJy5zZWxlY3RlZCcpLmVhY2goZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgZGlwbG9tYXMucHVzaCgkKHRoaXMpLmF0dHIoJ2RhdGEtZGlwbG9tYScpKTtcbiAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coZGlwbG9tYXMpXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vJCgnI2luZm8nKS50ZXh0KCdMYSByw6lwYXJ0aXRpb24gaW50ZXJuYXRpb25hbGxlIGRlcyAnKTtcbiAgICAgICAgICAgIC8vJCgnI2luZm8nKS5hcHBlbmQoIGRpcGxvbWFzICtcIiBlbiAyMDE1XCIpO1xuXG4gICAgICAgICAgICBpZiggZWxlbWVudFswXSAhPT0gdW5kZWZpbmVkICl7IFxuICAgICAgICAgICAgICAgICQoJyNpbmZvJykudGV4dCgnTGEgcsOpcGFydGl0aW9uIGludGVybmF0aW9uYWwgZGVzICcpO1xuICAgICAgICAgICAgICAgICQoJyNpbmZvJykuYXBwZW5kKCBkaXBsb21hcyArXCIgZW4gMjAxNVwiKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICBcblxuXG5cbiAgICB9LCAgcmVuZGVyQW5pbWF0aW9uKCkge1xuICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciBmbGFnID0gMDtcbiAgICAgICAgICAgIHZhciBmbGFnMiA9IDA7XG5cbiAgICAgICAgICAgICQoJyNwaWN0b2FkZCcpLmNsaWNrKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgZmxhZyA9IDE7XG5cbiAgICAgICAgICAgICAgICAkKCdib2R5JykuY3NzKCBcImJhY2tncm91bmQtaW1hZ2VcIiwgXCJub25lXCIgKTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBpZihmbGFnMiA9PTApe1xuICAgICAgICAgICAgICAgICAgICAkKCcjbG9naW4nKS5mYWRlSW4oKTtcbiAgICAgICAgICAgICAgICAgICAgZmxhZzIgPSAxO1xuICAgICAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgICAgICAkKCcjbG9naW4nKS5mYWRlT3V0KCk7XG4gICAgICAgICAgICAgICAgICAgICQoJ2JvZHknKS5jc3MoIFwiYmFja2dyb3VuZC1pbWFnZVwiLCBcInVybCgnLi4vc3JjL3ZlbmRvci9hc3NldHMvYmFja2dyb3VuZC5wbmcnKVwiICk7XG4gICAgICAgICAgICAgICAgICAgICQoJyNwaWN0b2FkZCcpLmZhZGVPdXQoKTtcbiAgICAgICAgICAgICAgICAgICAgJCgnbWFpbicpLmZhZGVJbigyMDAwKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJwaWN0b1wiKTtcblxuICAgICAgICAgICAgfSk7XG5cblxuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZihmbGFnID09IDApe1xuICAgICAgICAgICAgICAgICQoZG9jdW1lbnQpLmNsaWNrKGZ1bmN0aW9uKCl7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKCFldmVudC5wcmV2ZW50RGVmYXVsdCgpICYmIGZsYWcgPT0gMSkgcmV0dXJuOyBcblxuICAgICAgICAgICAgICAgICAgICBmbGFnID0gMTtcbiAgICAgICAgICAgICAgICAgICAgJCgnYm9keScpLmNzcyggXCJiYWNrZ3JvdW5kLWltYWdlXCIsIFwidXJsKCcuLi9zcmMvdmVuZG9yL2Fzc2V0cy9iYWNrZ3JvdW5kLnBuZycpXCIgKTtcbiAgICAgICAgICAgICAgICAgICAgJCgnI3BpY3RvYWRkJykuZmFkZU91dCgpO1xuICAgICAgICAgICAgICAgICAgICAkKCdtYWluJykuZmFkZUluKDIwMDApO1xuICAgICAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKGZsYWcpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcblxuICAgICAgICAgICBcblxuXG5cblxuICAgIH1cblxuXG5cblxufSk7XG5cbmV4cG9ydCBkZWZhdWx0IHBlcnNvbm5WaWV3O1xuXG4iLCJpbXBvcnQgcGVyc29ubmVWaWV3IGZyb20gJy4vcGVyc29ubmVzL3BlcnNvbm5lVmlldyc7XG5cblxuXG4kKGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgdmlldyA9IG5ldyBwZXJzb25uZVZpZXcoKTtcbiB9KTtcblxuXG5cblxuXG4iXX0=
